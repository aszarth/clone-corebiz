import Icon from "./Icon";
import Input from "./Input";
import SelectOption from "./SelectOption";

export default function ContactPreFooter({ isMobile }: { isMobile: boolean }) {
  return (
    <section
      style={{
        backgroundColor: "#F5F5F5",
        height: isMobile ? 1200 : 650,
        paddingTop: isMobile ? undefined : 30,
        fontFamily: "Circular Std Bold,sans-serif",
        display: "flex",
        flexDirection: isMobile ? "column" : "row",
        padding: isMobile ? "0% 5%" : undefined,
        fontWeight: 300,
        // alignItems: "center",
        justifyContent: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignSelf: "flex-start",
          height: "100%",
          width: 400,
          marginRight: 100,
        }}
      >
        <h2
          style={{
            fontSize: isMobile ? 26 : 32,
            padding: "0% 3%",
            fontWeight: "bold",
            marginTop: 90,
          }}
        >
          quer aumentar suas vendas online?
        </h2>
        <h3
          style={{
            fontSize: isMobile ? 16 : 14,
            marginTop: isMobile ? 20 : 15,
            padding: "0% 3%",
            marginRight: 10,
          }}
        >
          Entregamos projetos de E-commerce de acordo com as necessidades de sua
          empresa. Vamos conversar!
        </h3>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <div
          className="pcNameMail"
          style={{
            display: "flex",
            flexDirection: !isMobile ? "row" : "column",
          }}
        >
          <Input
            placeholder="Para começar, Qual seu Nome?"
            width={isMobile ? "100%" : 400}
            height={50}
            style={{ marginTop: 30, marginRight: !isMobile ? 10 : 0 }}
          />
          <Input
            placeholder="Seu e-mail corporativo"
            width={isMobile ? "100%" : 300}
            height={50}
            style={{
              marginTop: isMobile ? 15 : 30,
              marginLeft: !isMobile ? 10 : 0,
            }}
          />
        </div>
        <div
          className="pcSecondLine"
          style={{
            display: "flex",
            flexDirection: !isMobile ? "row" : "column",
          }}
        >
          <div className="pcLeftInputs" style={{ marginRight: 10 }}>
            <SelectOption.Container
              placeholder="Seu cargo"
              width={isMobile ? "100%" : 300}
              height={50}
              style={{ marginTop: 15 }}
            >
              <SelectOption.Item value="dono">Proprietário</SelectOption.Item>
              <SelectOption.Item value="presidente">
                Presidente ou C-Level
              </SelectOption.Item>
              <SelectOption.Item value="diretor">Diretor</SelectOption.Item>
              <SelectOption.Item value="gerente">Gerente</SelectOption.Item>
              <SelectOption.Item value="coordenador">
                Coordenador
              </SelectOption.Item>
              <SelectOption.Item value="analista">Analista</SelectOption.Item>
              <SelectOption.Item value="estagiario">
                Estagiário
              </SelectOption.Item>
              <SelectOption.Item value="outro">Outro</SelectOption.Item>
            </SelectOption.Container>
            <Input
              placeholder="Seu Celular?"
              width={isMobile ? "100%" : 300}
              height={50}
              style={{ marginTop: 15 }}
              isPhoneNumber={true}
            />
            <Input
              placeholder="Sua empresa"
              width={isMobile ? "100%" : 300}
              height={50}
              style={{ marginTop: 15 }}
            />
            <SelectOption.Container
              placeholder="Seu país"
              width={isMobile ? "100%" : 300}
              height={50}
              style={{ marginTop: 15 }}
            >
              <SelectOption.Item value="argetina">Argentina</SelectOption.Item>
              <SelectOption.Item value="brasil">Brasil</SelectOption.Item>
              <SelectOption.Item value="chile">Chile</SelectOption.Item>
              <SelectOption.Item value="usa">Estados Unidos</SelectOption.Item>
              <SelectOption.Item value="mexico">México</SelectOption.Item>
              <SelectOption.Item value="espanha">Espanha</SelectOption.Item>
              <SelectOption.Item value="outro">Outro</SelectOption.Item>
            </SelectOption.Container>
            <SelectOption.Container
              placeholder="Número de funcionários"
              width={isMobile ? "100%" : 300}
              height={50}
              style={{ marginTop: 15 }}
            >
              <SelectOption.Item value="1-20">1-20</SelectOption.Item>
              <SelectOption.Item value="20-50">20-50</SelectOption.Item>
              <SelectOption.Item value="50-100">50-100</SelectOption.Item>
              <SelectOption.Item value="100-250">100-250</SelectOption.Item>
              <SelectOption.Item value="250-500">250-500</SelectOption.Item>
              <SelectOption.Item value="500-1000">500-1000</SelectOption.Item>
              <SelectOption.Item value="1000+">Mais de 1.000</SelectOption.Item>
            </SelectOption.Container>
            <SelectOption.Container
              placeholder="Seu desafio"
              width={isMobile ? "100%" : 300}
              height={50}
              style={{ marginTop: 15 }}
            >
              <SelectOption.Item value="criar_um_ecommerce">
                Criar um e-commerce
              </SelectOption.Item>
              <SelectOption.Item value="gestao_em_marketing_digital">
                Gestão em marketing digital
              </SelectOption.Item>
              <SelectOption.Item value="evolucao_do_ecommerce">
                Evolução do e-commerce
              </SelectOption.Item>
              <SelectOption.Item value="aumento_da_conversao">
                Aumento da conversão
              </SelectOption.Item>
              <SelectOption.Item value="criacao_appmobile">
                Criação de app/mobile
              </SelectOption.Item>
              <SelectOption.Item value="midia_e_performance">
                Campanhas de mídia e performance
              </SelectOption.Item>
              <SelectOption.Item value="outros">Outros</SelectOption.Item>
            </SelectOption.Container>
          </div>
          <div className="pcRightInputs" style={{ marginLeft: 10 }}>
            <Input
              placeholder="Deixe uma mensagem"
              width={isMobile ? "100%" : 400}
              height={!isMobile ? 290 : 150}
              style={{ marginTop: 15 }}
            />
            <button
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                marginTop: 50,
                backgroundColor: "#000",
                width: isMobile ? "100%" : 400,
                height: 50,
                borderRadius: 50,
                cursor: "pointer",
              }}
              onClick={() => console.log("gogogo")}
            >
              <span
                style={{
                  fontSize: 18,
                  fontFamily: "Circular Std Bold,sans-serif",
                  fontWeight: "bold",
                  color: "#FFF",
                }}
              >
                receber contato comercial
              </span>

              <Icon
                name="angle-right"
                width={20}
                height={20}
                color="#FFF"
                style={{ marginLeft: 10 }}
              />
            </button>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginTop: 15,
                width: isMobile ? "100%" : 400,
                paddingBottom: isMobile ? 90 : 0,
              }}
            >
              <input type="checkbox" style={{ width: 10, height: 10 }} />
              <span
                style={{
                  fontFamily: "Circular Std Medium,sans-serif",
                  fontSize: 10,
                  marginLeft: 10,
                  fontWeight: "bold",
                }}
              >
                Ao me cadastrar concordo em receber novidades sobre e-commerce,
                varejo e eventos relacionados à Corebizz
              </span>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
