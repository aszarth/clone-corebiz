"use client";
import CheckIsMobile from "@/libs/checkIsMobile";
import { useEffect, useState } from "react";

export default function Hero({
  title,
  subtitle,
  imgSrc,
  buttonTxt,
}: {
  title: string;
  subtitle: string;
  imgSrc: string;
  buttonTxt: string;
}) {
  // mobile detect
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    setIsMobile(CheckIsMobile());
  }, []);
  return (
    <header
      style={{
        width: "100vw",
        backgroundColor: "#000",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <div
        style={{
          width: !isMobile ? 1300 : undefined,
          display: "flex",
          flexDirection: !isMobile ? "row" : "column",
          justifyContent: "space-between",
          alignItems: "center",
          backgroundColor: "#000",
          height: !isMobile ? 400 : 600,
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginLeft: 15,
            marginBottom: !isMobile ? 50 : 0,
            marginTop: isMobile ? 25 : 0,
          }}
        >
          <h2
            style={{
              fontSize: !isMobile ? 75 : 50,
              fontWeight: "bold",
              lineHeight: 1,
              maxWidth: 500,
              color: "#FFF",
            }}
          >
            {title}
          </h2>
          <h3
            style={{
              marginTop: 15,
              fontSize: !isMobile ? 18 : 16,
              maxWidth: 500,
              color: "#FFF",
            }}
          >
            {subtitle}
          </h3>
        </div>
        <div
          style={{
            marginRight: !isMobile ? 100 : 0,
            marginBottom: !isMobile ? 50 : 0,
            paddingBottom: isMobile ? 50 : 0,
          }}
        >
          <img src={imgSrc} width={400} height={400} />
        </div>
      </div>
    </header>
  );
}
