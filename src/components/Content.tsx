export default function Content({ children }: { children: React.ReactNode }) {
  return <div style={{ minHeight: 500 }}>{children}</div>;
}
