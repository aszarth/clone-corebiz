"use client";
import { useState } from "react";
import Icon from "./Icon";

export default function NavHeader({ isMobile }: { isMobile: boolean }) {
  return <header>{!isMobile ? <NavHeaderPC /> : <NavHeaderMobile />}</header>;
}

function NavHeaderPC() {
  return (
    <div
      style={{
        width: "100vw",
        backgroundColor: "#000",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <nav
        style={{
          backgroundColor: "#000",
          width: "100vw",
          height: 100,
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          maxWidth: 1300,
        }}
      >
        <h2 style={{ display: "none" }}>Barra de Navegação do Header</h2>
        <img
          src="/logo-corebiz-global.svg"
          width={105}
          height={25}
          style={{ marginLeft: 25 }}
          alt="logo da empresa"
        />
        <div
          className="rightSide"
          style={{
            marginRight: 25,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <ul
            className="navItems"
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <NavHeaderItem isSelected={false}>serviços</NavHeaderItem>
            <NavHeaderItem isSelected={false}>app commerce</NavHeaderItem>
            <NavHeaderItem isSelected={true}>cases</NavHeaderItem>
            <NavHeaderItem isSelected={false}>contato</NavHeaderItem>
            <NavHeaderItem isSelected={false}>careers</NavHeaderItem>
            <NavHeaderItem isSelected={false}>blog</NavHeaderItem>
          </ul>
          <div
            className="idiomItems"
            style={{ marginLeft: 10, marginRight: 10 }}
          >
            <NavHeaderIdioms isMobile={false} />
          </div>
        </div>
      </nav>
    </div>
  );
}

function NavHeaderMobile() {
  return (
    <div style={{ height: 49 }}>
      <div
        style={{
          height: 50,
          backgroundColor: "#000",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            height: 50,
            marginLeft: 10,
          }}
        >
          <Icon
            name="bars"
            width={16}
            height={16}
            color="#FFF"
            style={{ marginLeft: 10 }}
          />
          <img
            src="/logo-corebiz-global.svg"
            width={105}
            height={25}
            style={{ marginBottom: 5, marginLeft: 25 }}
          />
        </div>
        <NavHeaderIdioms isMobile={true} />
      </div>
    </div>
  );
}

function NavHeaderItem({
  isSelected,
  children,
}: {
  isSelected: boolean;
  children: string;
}) {
  const [isMouseHover, setIsMouseHover] = useState(false);
  return (
    <li
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        cursor: "pointer",
      }}
      onMouseEnter={() => setIsMouseHover(true)}
      onMouseLeave={() => setIsMouseHover(false)}
    >
      <a
        style={{
          fontSize: 16,
          fontFamily: "Circular Std Book,sans-serif",
          color: isMouseHover ? "#c0c0c0" : "#FFF",
          transition: "color 500ms",
          marginRight: 30,
          marginTop: isSelected ? 4 : 0,
        }}
      >
        {children}
      </a>
      {isSelected && <NavHeaderBallSelected style={{ marginRight: 30 }} />}
    </li>
  );
}

function NavHeaderIdioms({ isMobile }: { isMobile: boolean }) {
  const [isMouseHoverPT, setIsMouseHoverPT] = useState(false);
  const [isMouseHoverES, setIsMouseHoverES] = useState(false);
  const [isMouseHoverEN, setIsMouseHoverEN] = useState(false);
  return (
    <ul
      style={{
        fontFamily: "Circular Std Bold,sans-serif",
        fontSize: 12,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginRight: isMobile ? 10 : 0,
      }}
    >
      <li
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          cursor: "pointer",
        }}
      >
        <a
          style={{
            color: isMouseHoverPT ? "#c0c0c0" : "#FFF",
            transition: "color 500ms",
            marginLeft: 15,
            marginTop: 4,
          }}
          onMouseEnter={() => setIsMouseHoverPT(true)}
          onMouseLeave={() => setIsMouseHoverPT(false)}
        >
          Português
        </a>
        <NavHeaderBallSelected style={{ marginLeft: 20 }} />
      </li>
      <li>
        <a
          style={{
            color: isMouseHoverEN ? "#fff" : "#c0c0c0",
            transition: "color 500ms",
            marginLeft: 15,
            cursor: "pointer",
          }}
          onMouseEnter={() => setIsMouseHoverEN(true)}
          onMouseLeave={() => setIsMouseHoverEN(false)}
        >
          En
        </a>
      </li>
      <li>
        <a
          style={{
            color: isMouseHoverES ? "#fff" : "#c0c0c0",
            transition: "color 500ms",
            marginLeft: 15,
            cursor: "pointer",
          }}
          onMouseEnter={() => setIsMouseHoverES(true)}
          onMouseLeave={() => setIsMouseHoverES(false)}
        >
          Es
        </a>
      </li>
    </ul>
  );
}

function NavHeaderBallSelected({ style }: { style: object }) {
  return (
    <div
      style={{
        width: 4,
        height: 4,
        borderRadius: 2,
        backgroundColor: "#FFF",
        marginTop: 1,
        ...style,
      }}
    ></div>
  );
}
