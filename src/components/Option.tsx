import React, { useContext } from "react";
import { SelectContext } from "./SelectOption";

export default function Option({
  value,
  children,
}: {
  value: string;
  children: string;
}) {
  const { isInsideSelect } = useContext(SelectContext);
  ValidateBlockedContainer(isInsideSelect);
  return <option value={value}>{children}</option>;
}

function ValidateBlockedContainer(isInsideSelect: boolean) {
  if (!isInsideSelect) {
    throw new Error("Option component only accepts Select as container");
  }
}
