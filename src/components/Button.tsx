"use client";
import { useState } from "react";

export default function Button({
  type,
  children,
  width,
  height,
  style,
  onClick,
  borderRadius,
}: {
  type: "primary" | "secondary";
  children: string;
  width: number | string;
  height: number | string;
  style?: object;
  onClick: Function;
  borderRadius?: number;
}) {
  const [primaryColor, setPrimaryColor] = useState("#2680eb");
  const [isMouseHover, setIsMouseHover] = useState(false);
  return (
    <button
      className="className"
      style={{
        width: width,
        height: height,
        backgroundColor:
          type == "primary" || isMouseHover ? primaryColor : "#FFF",
        color: type == "primary" || isMouseHover ? "#FFF" : primaryColor,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center",
        borderWidth: 2,
        borderStyle: "solid",
        borderColor: primaryColor,
        cursor: "pointer",
        fontSize: 14,
        borderRadius: borderRadius ? borderRadius : 3,
        ...style,
      }}
      onMouseEnter={() => {
        setIsMouseHover(true);
        if (type == "primary") setPrimaryColor("#2273d3");
      }}
      onMouseLeave={() => {
        setIsMouseHover(false);
        if (type == "primary") setPrimaryColor("#2680eb");
      }}
      onClick={() => onClick()}
    >
      {children}
    </button>
  );
}
