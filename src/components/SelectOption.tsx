import React from "react";
import Select from "./Select";
import Option from "./Option";

const SelectOption = {
  Container: Select, // Root
  Item: Option,
};
export default SelectOption;

// block option pra ser usado so dentro de select
interface SelectContextType {
  isInsideSelect: boolean;
}
export const SelectContext = React.createContext<SelectContextType>({
  isInsideSelect: false,
});
