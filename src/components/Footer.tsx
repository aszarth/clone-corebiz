"use client";
import ContactPreFooter from "./ContactPreFooter";
import Icon from "./Icon";
import style from "./Footer.module.scss";

export default function Footer({ isMobile }: { isMobile: boolean }) {
  return (
    <footer>
      <>
        <ContactPreFooter isMobile={isMobile} />
        <section className={style.containerFooter}>
          <h2 style={{ display: "none" }}>Footer do site</h2>
          <div className={style.pcFirstLine}>
            <CountriesContact />
            <OurCompany isMobile={isMobile} />
          </div>
          <div className={style.pcSecondLine}>
            <FollowUs />
            {isMobile && <ButtonToTop />}
            <OurBrands />
            <div>
              <IdiomsSelectFooter />
              <AllRights />
            </div>
            {!isMobile && <ButtonToTop />}
          </div>
        </section>
      </>
    </footer>
  );
}

function CountriesContact() {
  return (
    <section className={style.CountriesContact}>
      <h3 style={{ display: "none" }}>Contatos</h3>
      <CountriesContactItem
        name="Brasil"
        mail="brasil@corebiz.ag"
        phone="+55 11 3090 1039"
      />
      <CountriesContactItem name="Argentina" mail="argentina@corebiz.ag" />
      <CountriesContactItem
        name="Mexico"
        mail="mexico@corebiz.ag"
        phone="+52 55 7389 6086"
      />
      <CountriesContactItem name="Chile" mail="chile@corebiz.ag" />
      <CountriesContactItem name="España" mail="esp@corebiz.ag" />
    </section>
  );
}

function CountriesContactItem({
  name,
  mail,
  phone,
}: {
  name: string;
  mail: string;
  phone?: string;
}) {
  return (
    <section className={style.CountriesContactItem}>
      <h4 className={style.itemName}>{name}</h4>
      <p className={style.itemMail}>{mail}</p>
      <p className={style.itemPhone}>{phone}</p>
    </section>
  );
}

function OurCompany({ isMobile }: { isMobile: boolean }) {
  return (
    <section className={style.OurCompany}>
      {/* <img src="/logo_footer_pc.png" width={150} height={120} /> */}
      <img
        src={isMobile ? "/logo_footer_mobile.png" : "/logo_footer_pc.png"}
        width={isMobile ? 80 : 160}
        height={isMobile ? 60 : 120}
      />
      <h3
        style={{
          maxWidth: 290,
          fontSize: 17,
          fontWeight: 700,
          textAlign: "center",
        }}
      >
        A member of the <u>WPP</u> network and a <u>VML</u> company
      </h3>
    </section>
  );
}

function FollowUs() {
  return (
    <section className={style.FollowUs}>
      <h3>Siga a gente</h3>
      <div className={style.IconsContainer}>
        <Icon
          name="instagram"
          width={20}
          height={20}
          color="#FFF"
          className={style.icon}
        />
        <Icon
          name="facebook"
          width={20}
          height={20}
          color="#FFF"
          className={style.icon}
        />
        <Icon
          name="linkedin"
          width={20}
          height={20}
          color="#FFF"
          className={style.icon}
        />
      </div>
    </section>
  );
}

function ButtonToTop() {
  return (
    <button
      className={style.ButtonToTop}
      title="seta botão pra clicar pra e ir pro topo da página"
    >
      <img
        src="/2top.png"
        width={40}
        height={62}
        onClick={() => {
          window.scrollTo({ top: 0, behavior: "smooth" });
        }}
        alt="seta pra cima"
      />
      <span
        onClick={() => {
          window.scrollTo({ top: 0, behavior: "smooth" });
        }}
      >
        topo
      </span>
    </button>
  );
}

function OurBrands() {
  return (
    <section className={style.OurBrands}>
      <h3>Nossas marcas</h3>
      <div className={style.logosContainer}>
        <img src="/logo-trinto.png" width={70} height={40} />
        <img src="/logo-mobfiq-1.png" width={70} height={40} />
      </div>
    </section>
  );
}

function IdiomsSelectFooter() {
  return (
    <section className={style.IdiomsSelectFooter}>
      <h3 style={{ display: "none" }}>Escolher Idiomas (footer)</h3>
      <ul
        style={{
          width: 180,
          height: 50,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <li className={style.idiomSelected}>Portugues</li>
        <li className={style.idiomUnselected}>En</li>
        <li className={style.idiomUnselected}>Es</li>
      </ul>
    </section>
  );
}

function AllRights() {
  return (
    <section className={style.AllRights}>
      <h3>
        direitos reservados. <strong>corebiz</strong> 2022
      </h3>
    </section>
  );
}
