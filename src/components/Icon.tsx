"use client";
import SvgGithub from "./svgs/github.svg";
import SvgInternet from "./svgs/internet-explorer.svg";
import SvgInstagram from "./svgs/instagram.svg";
import SvgLinkedin from "./svgs/linkedin.svg";
import SvgFacebook from "./svgs/facebook-f.svg";
import SvgAngleRight from "./svgs/angle-right.svg";
import SvgAngleDown from "./svgs/angle-down.svg";
import SvgBars from "./svgs/bars.svg";

export default function Icon({
  name,
  width,
  height,
  color,
  style,
  onClick,
  className,
}: {
  name:
    | "internet"
    | "git"
    | "instagram"
    | "linkedin"
    | "facebook"
    | "angle-right"
    | "angle-down"
    | "bars";
  width: number;
  height: number;
  color: string;
  style?: object;
  onClick?: Function;
  className?: string;
}) {
  return (
    <div
      style={{ width: width, height: height, ...style }}
      onClick={() => {
        if (onClick) onClick();
      }}
      className={className}
    >
      {name == "internet" && (
        <SvgInternet fill={color} width={width} height={height} />
      )}
      {name == "git" && (
        <SvgGithub fill={color} width={width} height={height} />
      )}
      {name == "linkedin" && (
        <SvgLinkedin fill={color} width={width} height={height} />
      )}
      {name == "instagram" && (
        <SvgInstagram fill={color} width={width} height={height} />
      )}
      {name == "facebook" && (
        <SvgFacebook fill={color} width={width} height={height} />
      )}
      {name == "angle-right" && (
        <SvgAngleRight fill={color} width={width} height={height} />
      )}
      {name == "angle-down" && (
        <SvgAngleDown fill={color} width={width} height={height} />
      )}
      {name == "bars" && <SvgBars fill={color} width={width} height={height} />}
    </div>
  );
}
