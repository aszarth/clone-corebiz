import React, { useState, useRef } from "react";
import "./Input.css";

export default function CustomInput({
  placeholder,
  width,
  height,
  style,
  isPhoneNumber,
}: {
  placeholder: string;
  width: number | string;
  height: number;
  style?: object;
  isPhoneNumber?: boolean;
}) {
  const [value, setValue] = useState("");
  const [isInputSelected, setIsInputSelected] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);
  const textareaRef = useRef<HTMLTextAreaElement>(null);
  const handleFocus = () => {
    if (height > 50 && textareaRef.current) {
      textareaRef.current.focus();
    } else if (inputRef.current) {
      inputRef.current.focus();
    }
    setIsInputSelected(true);
  };
  return (
    <div
      style={{
        width: width,
        height: height,
        borderBottomWidth: isInputSelected ? 4 : 3,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderColor: isInputSelected ? "#000" : "rgba(0,0,0,.15)",
        borderStyle: "solid",
        ...style,
      }}
      onClick={handleFocus}
    >
      {height > 50 ? (
        <textarea
          ref={textareaRef}
          placeholder={placeholder}
          className={
            isInputSelected ? "input-placeholder-focused" : "input-placeholder"
          }
          value={value}
          onChange={(e) => {
            if (isPhoneNumber) {
              setValue(formatPhoneNumber(e.target.value));
            } else {
              setValue(e.target.value);
            }
          }}
          style={{
            width: "100%",
            height: "100%",
            border: "none",
            outline: "none",
            background: "transparent",
            color: "#000",
            fontSize: isInputSelected ? 18 : 16,
            fontWeight: isInputSelected ? "bold" : "none",
            padding: "0 5px",
            fontFamily: "Circular Std Bold,sans-serif",
          }}
          onBlur={() => {
            setIsInputSelected(false);
          }}
        />
      ) : (
        <input
          ref={inputRef}
          placeholder={placeholder}
          className={
            isInputSelected ? "input-placeholder-focused" : "input-placeholder"
          }
          value={value}
          onChange={(e) => {
            if (isPhoneNumber) {
              setValue(formatPhoneNumber(e.target.value));
            } else {
              setValue(e.target.value);
            }
          }}
          style={{
            width: "100%",
            height: "100%",
            border: "none",
            outline: "none",
            background: "transparent",
            color: "#000",
            fontSize: isInputSelected ? 18 : 16,
            fontWeight: isInputSelected ? "bold" : "none",
            padding: "0 5px",
            fontFamily: "Circular Std Bold,sans-serif",
          }}
          onBlur={() => {
            setIsInputSelected(false);
          }}
        />
      )}
    </div>
  );
}

const formatPhoneNumber = (value: string) => {
  if (!value) return value;
  const phoneNumber = value.replace(/[^\d]/g, "");
  const phoneNumberLength = phoneNumber.length;
  if (phoneNumberLength < 3) return phoneNumber;
  if (phoneNumberLength < 7) {
    return `(${phoneNumber.slice(0, 2)}) ${phoneNumber.slice(2)}`;
  }
  return `(${phoneNumber.slice(0, 2)}) ${phoneNumber.slice(
    2,
    7
  )}-${phoneNumber.slice(7, 11)}`;
};
