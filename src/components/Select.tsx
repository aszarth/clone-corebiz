import React, { useEffect, useRef, useState } from "react";
import Option from "./Option";
import SelectOption, { SelectContext } from "./SelectOption";
import "./Select.css";
import Icon from "./Icon";

export default function Select({
  width,
  height,
  style,
  children,
  placeholder,
}: {
  width: number | string;
  height: number | string;
  style?: object;
  children: React.ReactNode;
  placeholder: string;
}) {
  ValidateBlockedChildren(children);
  const [isFocused, setIsFocused] = useState(false);
  const [selectSomething, setSelectSomething] = useState(false);
  // icon pos (pq pode ser string)
  const [iconPosition, setIconPosition] = useState<number | null>(null);
  const containerRef = useRef<HTMLSelectElement>(null);
  useEffect(() => {
    const calculateIconPosition = () => {
      if (typeof width === "number") {
        setIconPosition(width - 20);
      } else if (containerRef.current) {
        const containerWidth = containerRef.current.offsetWidth;
        setIconPosition(containerWidth - 20);
      }
    };

    calculateIconPosition();

    window.addEventListener("resize", calculateIconPosition);
    return () => {
      window.removeEventListener("resize", calculateIconPosition);
    };
  }, [width]);
  //
  return (
    <SelectContext.Provider value={{ isInsideSelect: true }}>
      <select
        ref={containerRef}
        onFocus={() => {
          setIsFocused(true);
        }}
        onBlur={() => {
          setIsFocused(false);
        }}
        onChange={(event) => {
          if (event.target.value != "placeholder") {
            setSelectSomething(true);
          } else {
            setSelectSomething(false);
          }
        }}
        className="custom-select"
        style={{
          width: width,
          height: height,
          //
          paddingLeft: 5,
          color: isFocused || selectSomething ? "#000" : "rgba(0, 0, 0, 0.15)",
          fontSize: isFocused ? 18 : 16,
          fontWeight: isFocused || selectSomething ? "bold" : "normal",
          // border
          borderBottomWidth: isFocused ? 4 : 3,
          borderTopWidth: 0,
          borderRightWidth: 0,
          borderLeftWidth: 0,
          borderColor: isFocused ? "#000" : "rgba(0,0,0,.15)",
          borderStyle: "solid",
          ...style,
        }}
      >
        <SelectOption.Item value="placeholder">{placeholder}</SelectOption.Item>
        {children}
      </select>
      <Icon
        name="angle-down"
        width={16}
        height={16}
        color={isFocused ? "#000" : "rgba(0,0,0,.15)"}
        style={{
          position: "relative",
          bottom: 35,
          left: iconPosition,
        }}
      />
    </SelectContext.Provider>
  );
}

function ValidateBlockedChildren(children: React.ReactNode) {
  React.Children.forEach(children, (child) => {
    if (!React.isValidElement(child) || child.type !== Option) {
      throw new Error("Select component only accepts Option as children");
    }
  });
}
