import Hero from "@/components/Hero";

export default function Cases() {
  return (
    <main>
      <Hero title="title" subtitle="desc" imgSrc="/img.png" buttonTxt="btn" />
      <span>Cases</span>
    </main>
  );
}
