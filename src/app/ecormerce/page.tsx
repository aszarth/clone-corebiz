import Hero from "@/components/Hero";

export default function Ecomerce() {
  return (
    <main>
      <Hero title="title" subtitle="desc" imgSrc="/img.png" buttonTxt="btn" />
      <span>Ecomerce</span>
    </main>
  );
}
