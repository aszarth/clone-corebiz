// import styles from "./page.module.scss";

"use client";
import Hero from "@/components/Hero";
import styles from "./cases/page.module.scss";
import Button from "@/components/Button";
import { useEffect, useState } from "react";
import CheckIsMobile from "@/libs/checkIsMobile";

export default function Cases() {
  // mobile detect
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    setIsMobile(CheckIsMobile());
  }, []);
  return (
    <main>
      <Hero
        title="Cases de sucesso"
        subtitle="Saiba por que as maiores marcas de E-commerce da América Latina escolhem a Corebiz"
        imgSrc="/hero_cases.png"
        buttonTxt=""
      />
      <ClientsIntro />
      <ClientCardsContainer>
        <ClientCard
          img="/cases/motorola.png"
          title="Motorola escala suas operações D2C globais"
          content="A pioneira de comunicação móvel se juntou a Corebiz e VTEX para unificar seus canais B2B e D2C e conquistou nada menos que um crescimento mensal de 25% na taxa de conversão. Confira o case completo neste artigo."
          isMobile={isMobile}
          imgPosition="left"
        />
        <ClientCard
          img="/cases/carrefour.png"
          title="Carrefour acelera estratégia digital com a Corebiz"
          content="Em parceria com a Corebiz, Carrefour otimiza a performance e acelera as vendas do seu e-commerce. Com a reestruturação de todo o sistema de check out dos canais digitais, a multinacional teve um aumento na conversão e um maior engajamento dos usuários na utilização do cartão da loja."
          isMobile={isMobile}
          imgPosition="right"
        />
        <ClientCard
          img="/cases/whirlpool.png"
          title="Grupo Whirlpool expande estratégia global de e-commerce"
          content="A Whirlpool escolheu a Corebiz para desenvolver sua plataforma de E-commerce com o propósito de impulsionar as vendas online e padronizar os canais na Europa, América Latina, Oriente Médio e África."
          isMobile={isMobile}
          imgPosition="left"
        />
      </ClientCardsContainer>
    </main>
  );
}

function ClientsIntro() {
  return (
    <section className={styles.ClientsIntro}>
      <h2>Somos referência na Europa e na América Latina</h2>
      <div className={styles.itemsContainer}>
        <div className={styles.item}>
          <p className={styles.title}>+140</p>
          <h3 className={styles.desc}>clientes</h3>
        </div>
        <div className={styles.item}>
          <p className={styles.title}>+40</p>
          <h3 className={styles.desc}>países com projetos</h3>
        </div>
        <div className={styles.item}>
          <p className={styles.title}>98%</p>
          <h3 className={styles.desc}>dos clientes satisfeitos</h3>
        </div>
      </div>
    </section>
  );
}

function ClientCardsContainer({ children }: { children: React.ReactNode }) {
  return (
    <section className={styles.ClientCardsContainer}>
      <h2 style={{ display: "none" }}>Principais Clientes</h2>
      <div className={styles.items}>{children}</div>
    </section>
  );
}

function ClientCard({
  img,
  title,
  content,
  isMobile,
  imgPosition,
}: {
  img: string;
  title: string;
  content: string;
  isMobile: boolean;
  imgPosition: "right" | "left";
}) {
  return (
    <section className={styles.ClientCard}>
      {(isMobile || imgPosition == "left") && (
        <div className={styles.imgContainer}>
          <img
            src={img}
            width="100%"
            height="100%"
            loading="lazy"
            alt={`imagem cliente ${title}`}
          />
        </div>
      )}
      <div
        className={`${styles.sideContent} ${
          styles[`imgPosition_${imgPosition}`]
        }`}
      >
        <h3>{title}</h3>
        <p>{content}</p>
        <div className={styles.btnReadMore}>
          <Button
            type="secondary"
            width={!isMobile ? 240 : "90vw"}
            height={50}
            onClick={() => alert("oi")}
          >
            Leia o case completo
          </Button>
        </div>
      </div>
      {!isMobile && imgPosition == "right" && (
        <div className={styles.imgContainer}>
          <img
            src={img}
            width="100%"
            height="100%"
            loading="lazy"
            alt={`imagem cliente ${title}`}
          />
        </div>
      )}
    </section>
  );
}
