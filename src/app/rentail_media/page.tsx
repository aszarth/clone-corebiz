import Hero from "@/components/Hero";

export default function RentailMedia() {
  return (
    <main>
      <Hero title="title" subtitle="desc" imgSrc="/img.png" buttonTxt="btn" />
      <span>RentailMedia</span>
    </main>
  );
}
