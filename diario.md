# node need (no docker here)
`nvm use 18.17.0`

# start project
`npx create-next-app corebiz`

# install svg

`yarn add @svgr/webpack`

next.config.mjs
```js
// next.config.mjs
export default {
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
};
```

## Sass

next nativaly supports Sass so you just need to install it

```
yarn add -D sass
```
